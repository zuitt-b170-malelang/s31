// routes - contains all the endpoints for the application; instead of putting a lot of routes in the index.js, we separate them in other file, routes.js (databaseRoutes.js i.e. taskRoutes.js)

const express = require("express");

// creates a router instance that functions as a middleware and routing systemp; allows access to HTTP Methods middlewares that makes it easier to create routes for our application
const router = express.Router();

// allows us to use the controllers' functions
const taskController = require("../controllers/controllers.js")


router.get("/", (req,res) => {
  // taskController - a controller file that is existing in the repo
  // getAllTasks is a function that is encoded inside the taskController file
  // use .then to wait for the getAllTasks function to be executed before proceeding to the statements(res.send(result))
  taskController.getAllTasks().then(result=>res.send(result))
})

// route for creating a task
router.post("/", (req,res) =>{
  taskController.createTask(req.body).then(result => res.send(result));
})

/* 
  : - wildcard; if tagged with a parameter, the app will automatically look for the parameter that is in the url
*/
// localhost:3000/tasks/62e352d32412131
router.delete("/:id", (req,res) => {
  // URL parameter values are accessed through the request object's "params" property; the specified parameter/property of the object will match the URL parameter endpoint
  taskController.deleteTask(req.params.id).then(result=> res.send(result))
})

router.put("/:id", (req,res)=>{
  taskController.updateTask(req.params.id, req.body).then(result=>res.send(result));
})

router.get("/:id", (req,res) => {

  taskController.findId(req.params.id).then(result=>res.send(result))
})

router.put("/:id/complete", (req,res)=>{
  taskController.updateStatus(req.params.id, req.body.status).then(result=>res.send(result));
})


//module.exports - a way for js to treat the value/file as a package that can be imported/used by other files
module.exports = router;