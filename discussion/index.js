/* 
set up of dependencies

-load express and mongoose packages and store them inside the "express" and mongoose variables
-create app var and store express server
-create port
make the app able to read json and accept other types of data from forms
-connect your app tp mongodb b170 to do
set up confirmation of connectiong or not connecting to the database
-make the app listen top port and set up confirmation in the console that the "server is running at port"
*/

const express = require("express");
const mongoose = require("mongoose");

// load the package that is inside the routes.js file in the repo
const taskRoute = require("./routes/routes.js")
const app = express();
const port = 3000

mongoose.connect("mongodb+srv://evmalelang1:badminton05@wdc028-course-booking.jteye.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
useNewUrlParser: true,
useUnifiedTopology: true
})
let db = mongoose.connection;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", ()=> console.log("We're connected to the database"))

//gives the app an access to the routes needed
app.use("/tasks", taskRoute)



app.listen(port, () => console.log(`Server is running at port ${port}`))