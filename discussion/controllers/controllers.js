// containing the functions/statements/logic to be executed once a route has been triggered/entered
// responsible for execution of CRUD operations/methods

// create controller functions that responds to the routes

//to give access to the contents the for task.js file in the models folderl meaning it can use the task model
const { update } = require("../models/task.js");
const Task = require("../models/task.js")


// create controller funcitons that responds to the routes
module.exports.getAllTasks = () => {
  return Task.find({}).then(result=>{
    return result;
  })
}

module.exports.createTask = (requestBody) => {
  let newTask = new Task({
    name: requestBody.name
  })
  // saving mechanism
  /* 
    .then accepts 2 parameters
      -first parameter stores the result object; if we have successfully saved the object
      -second parameter stores the object, should there be one
  */
  return newTask.save().then((savedTask, error) =>{
    // error handling
    if(error){
      console.log(error);
      return false
    }else{
      return savedTask
    }
  })
}


/* 

*/
module.exports.deleteTask = (taskId) => {
  // findByIdandRemove - finds the item to be deleted and removes it from the database; it uses id in looking for the document
  return Task.findByIdAndRemove(taskId).then((removedTask, error) =>{
    if (error){
      console.log(error);
      return false
    }else{
      return removedTask
    }
  })
}

/* 
update task
1. to find the id (findByIdAndUpdate())
2. replace the task's name returned from the db with the name property of the requestBody
3. save the task
*/

module.exports.updateTask = (taskId, requestBody) => {

  return Task.findById(taskId).then((result, error) =>{
    if (error){
      console.log(error);
      return false
    }else{
      result.name = requestBody.name;
      return result.save().then((updateTask, error)=>{
        if(error){
          console.log(error)
          return false
        } else{
          return updateTask
        }
      })
    }
  })
}

// ACTIVITY
/* 
1. get a specific task in the database
BUSINESS LOGIC
"/:id"
-find the id (findById())
-return it as a response


2. update the status of a task
BUSINESS LOGIC
"/:id/complete"
-find the id of the task
-change the value of the status into "complete" using the requestBody
-save task
*/

// 1.
module.exports.findId = (taskId) =>{
  return Task.findById(taskId).then((result, error)=>{
    if (error){
      console.log(error)
      return false
    }else{
      return(result)
    }
  })
}


// 2.
module.exports.updateStatus = (taskId, requestBodyStatus) => {

  return Task.findById(taskId).then((result, error) =>{
    if (error){
      console.log(error);
      return false
    }else{
      result.status = requestBodyStatus;
      console.log(result)
      return result.save().then((updateStatus, error)=>{
        if(error){
          console.log(error)
          return false
        } else{
          return updateStatus
        }
      })
    }
  })
}